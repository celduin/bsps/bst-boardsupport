# BuildSteam Board Support Style Guide
Default to the following styles where possible for consistent elements.

## Lists
Top-level YAML lists are not indented, but other levels are. This is consistent
with the output from `bst track` and other BuildStream projects.

```yaml
depends:
- foo
- bar
  - baz
```

## Dependencies
Use the `build-depends` scalar and junction shorthand instead of long-form
junction references, because the short-form is easier to read and produces nicer
diffs.

Do:
```yaml
build-depends:
- freedesktop-sdk.bst:components/networkmanager.bst
```

Don't do:
```yaml
depends:
- filename: components/networkmanager.bst
  junction: freedesktop-sdk.bst
  type: build
```
