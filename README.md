# bst-boardsupport (Prototype)
A grab bag of BuildStream elements required to provide support for various
hardware targets (boards). Junction this project as you would freedesktop-sdk
to add board support to your BuildStream project.

## Audience
This is a systems integration project. System integrators who use this are
expected to have some knowledge of the following:
- BuildStream basics, familiarity with freedesktop-sdk etc
- Build tooling, eg. autotools, cmake, meson, make
- Linux kernel configuration and build
- rootfs image creation
- Bootloaders

Development of these projects is specifically out of scope, BuildStream consumes
existing code repos. Although, there may be some patching involved.

Familiarity with the following projects is a bonus, although we are specifically
**not** trying to recreate them:
- Buildroot
- Yocto

## Goals
BuildStream is designed to:
- Create tracable and cacheable software layers.
- Support massively parallelised workflows.

Codethink would like to recommend the use of BuildStream to embedded developers,
as a part of their "use mainline software" story. However there is a barrier to
entry since there are no existing "typical embdedded" projects that use
BuildStream.

The goal of this project is to:
1. Provide that reference in some form.
2. Provide a set of common generic elements (eg. linux.bst, u-boot.bst)

Specifically out of scope for BuildStream is (or unimplemented):
- Deployment to the target (uploading ostree artifacts or flashing a board, for
  example)
- Licensing
  https://gitlab.com/celduin/crash/buildstream-issue-tracker/-/issues/1

It is currently held that there are other tools which are better for these jobs.

## Mechansim
This project alone cannot be configured to produce a bootable image. However,
the elements inside it can be used in another project to add support for various
hardware targets.

The elements in the `elements/` directory can be used as-is, but if your project
needs further customisation (eg. alternative linux kernel configuration), then
it is possible to compose elements yourself using the generic elements and
board-specific data in `include/`. Some `sample/` projects and included.

This is a work in progress. Please add criticisms and contribute.

Alternative methods:
- Overlay, placeholder application in BSP is overlayed with main application.
- Include, all BSP elements are included into main application, via placeholders.

## Sample
The sample directory contains several BuildStream projects, which showcase
various features of this kind of project layout.

Is it **NOT** expected that end users will create projects in this repo. Rather,
each project in sample would be its own git repo somewhere else - which consumes
this board support project.

## Tips and Tricks
If you're new to BuildStream you might not be aware of the following time-saving
techniques.

### Showing variables
You can inspect some bst variables before pushing to CI:

    bst show --help
    bst show --format %{vars} deploy/image.bst

### Workspaces
You can inspect an element sandbox using the workspace feature:

    bst workspace --help
    bst workspace open deploy/image.bst ws-image
