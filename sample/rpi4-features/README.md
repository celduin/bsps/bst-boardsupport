# rpi4-features
A small project which assembles a basic rpi4 image with some applications to
demo features like graphics, wifi, bluetooth.

Features:
- Uses Raspberry Pi 4 "BSP"
- Uses minimal Freedesktop SDK filesystem layout
- Weston, glxgears
- NetworkManager
