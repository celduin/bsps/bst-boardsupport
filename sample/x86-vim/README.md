# x86-vim
A small project which assembles a basic x86_64 image with the Vim application.

Features:
- Uses x86_64 "BSP"
- Uses minimal Freedesktop SDK filesystem layout
- Vim

How to use:
 * To build the project:
   - bst track deploy/image.bst
   - bst build deploy/image.bst
   - bst checkout deploy/image.bst vm_image
 * To use the image created:
   - qemu-system-x86_64 -enable-kvm vm_image/disk.img
