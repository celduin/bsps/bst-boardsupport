# rpi4-vim
A small project which assembles a basic rpi4 image with the Vim application.

Features:
- Uses Raspberry Pi 4 "BSP"
- Uses minimal Freedesktop SDK filesystem layout
- Vim
